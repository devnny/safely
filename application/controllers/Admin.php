<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->form_validation->set_error_delimiters(' ', ' ');

		if($this->session->activo->rol == 'ADM')
		{
			//date_default_timezone_set('America/Mexico_City');
			$this->load->model('categorias');		
		}
		else
		{
			redirect('entrar');
		}
	}

	public function index()
	{
		
	}

	

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */ 