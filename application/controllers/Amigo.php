<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amigo extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->form_validation->set_error_delimiters(' ', ' ');

		if($this->session->activo->rol == 'FRIEND')
		{
			//date_default_timezone_set('America/Mexico_City');
			$this->load->model('categorias');		
		}
		else
		{
			redirect('entrar');
		}
	}

	public function index()
	{
		
	}

}

/* End of file Amigo.php */
/* Location: ./application/controllers/Amigo.php */ 