<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entrar extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->form_validation->set_error_delimiters(' ', ' ');

		$this->load->model('categorias');
	}

	public function index()
	{
		//redirect('inicio','refresh')
	}

	public function 

	public function registrar()
	{
		if($this->input->is_ajax_request())
		{

	        $this->form_validation->set_rules('mail', 'correo electrónico','required|is_unique[persona.correo]|valid_email|is_unique[usuario.username]');

			$this->form_validation->set_rules('pass', 'contraseña','required|max_length[20]|min_length[6]');
			$this->form_validation->set_rules('pass2', 'confirmación de contraseña','required|matches[pass]|max_length[20]');
			
			$this->form_validation->set_message('required', 'El campo %s es obligatorio');
			$this->form_validation->set_message('is_unique', 'El %s ya está registrado');
			$this->form_validation->set_message('matches', 'Las contraseñas no coinciden');
			$this->form_validation->set_message('min_length', 'El campo {field} debe contener mínimo {param} caracteres');
			$this->form_validation->set_message('max_length', 'El campo {field} debe contener máximo {param} caracteres');
			$this->form_validation->set_message('valid_email', 'Formato de correo erroneo');

			if ($this->form_validation->run())
			{
				$idPerRow = $this->categorias->ultimo_id('persona');
				if($idPerRow->id_persona != null)
				{
					$numeracion = str_replace('P0_','',$idPerRow->id_persona)+1;
	    			$zeros = '';
	    			for($i = strlen($numeracion); $i < 5; $i++){
	    				$zeros .= '0';
	    			}
	    			$id_persona ='P0_'.$zeros.$numeracion;
	    			$id_usuario ='U0FRI'.$zeros.$numeracion;
				}else{
					$id_persona = 'P0_00001';
	    			$id_usuario ='U0FRI00001';
				}

				//SE debe crear el enlace para activar la cuenta// 

				$caracteres = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-#!';
				$aleatoria = substr(str_shuffle($caracteres), 0, 6);

				$dataBitacora = array(
					'id_usuario'	=> 'WEB',
					'ip' 			=> $this->input->ip_address(),
					'id_accion' 	=> 'CRE',
					'descripcion' 	=> 'CREACIÓN DEL USUARIO con ID ' . $id_usuario
				);

				$dataPersona = array(
					'id_persona'  	=> $id_persona,
					'correo' 		=> $this->input->post('mail')
				);

				$dataUsuario = array(
					'id_usuario'	=> $id_usuario,
					'id_persona'  	=> $id_persona,
					'username' 		=> $this->input->post('mail'),
					'password' 		=> password_hash($this->input->post('pass'), PASSWORD_BCRYPT),
					'rol' 			=> 'FRIEND'
				);

				$dataClave = array(
					'clave_activacion'	=> $aleatoria,
					'fk_usuario'		=> $id_usuario,
					'clave_estado'		=> 0
				);

				$verifica = $this->categorias->existeUsuario($this->input->post('mail'));

				if($verifica['resultado'])
				{
					$result = $this->categorias->ingresarUsuarioWeb($dataPersona, $dataUsuario, $dataBitacora, $dataClave);

					if($result['resultado'])
					{
						$this->load->library('email');

						$this->email->from('sirmurdok@gmail.com', 'SAFELY');
						$this->email->to($this->input->post('mail'));

						$this->email->subject('Confirmación de Registro');
						$this->email->message('Gracias por registrarte en SAFELY, para poder continuar por favor confirma tu correo dando click en el siguiente enlace: '.base_url().'activacion/'.$aleatoria);

						//$this->email->send();
					}
					
					echo json_encode($result);
				}
				else
				{
					echo json_encode($verifica);
				}

			}
			else{
				$respuesta['resultado'] = FALSE;
			 	$respuesta['mensaje'] = 'Error en los datos';
				$respuesta['datos'] = validation_errors();

				echo json_encode($respuesta);
			}
		}
		else
		{
			redirect('');
		}
	}

}

/* End of file Entrar.php */
/* Location: ./application/controllers/Entrar.php */ 