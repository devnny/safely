<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function index()
	{
		$this->load->view('inicio_view');
	}

	public function login()
	{
		$this->load->view('login');
	}

	function login_form()
	{
		//Verifica que los datos ingresados sean válidos
		$this->form_validation->set_rules('usuario', 'correo electrónico', 'trim|required');
		$this->form_validation->set_rules('contrasenia', 'contraseña', 'trim|required');

		$this->form_validation->set_message('required', 'Debe ingresar su %s para iniciar sesión.');

		//Si se valida el fomulario sin problemas
		if ($this->form_validation->run())
		{
			$resultado = $this->loginlibrary->login($this->input->post('usuario'), $this->input->post('contrasenia'));

			if($resultado['estado'])
			{
				$id_usuario = $this->session->activo->id_usuario;
				$nombre = $this->session->activo->nombre;
                $ap = $this->session->activo->ap_paterno;
                $am = $this->session->activo->ap_materno;

		        $this->loginlibrary->insertarBitacora(array(
		        		'id_usuario'	=> $this->session->activo->id_usuario,
						'ip' 			=> $this->input->ip_address(),
						'id_accion' 	=> 'INI',
						'descripcion' 	=> 'USUARIO ' . $nombre . ' ' . $ap . ' ' . $am . '    INICIÓ SESIÓN'
		        	)
		        );

				$this->session->set_flashdata('correcto', 'Bienvenido/a ' . $this->session->activo->nombre);

				switch ($this->session->activo->rol)
				{
				    case 'ADM':
				        redirect('administrador', 'refresh');
				        break;
				    case 'FRIEND':
				        redirect('amigo', 'refresh');
				        break;
				    case 'BAN':
				        redirect('banco', 'refresh');
				        break;
				    default: 
				    	redirect('inicio');
				}
			}
			else
			{
				$this->session->set_flashdata('error', $resultado['mensaje']);
				$this->load->view('login');
			}
		}
		else
		{
			$this->load->view('login');
		}
	}

	function salir()
    {
    	if($this->session->activo->id_usuario != null)
    	{
	    	//Se llama la función de la libreria para cerrar sesión
	    	$dataBitacora = array(
				'id_usuario'	=> $this->session->activo->id_usuario,
				'ip' 			=> $this->input->ip_address(),
				'id_accion' 	=> 'CIE',
				'descripcion' 	=> 'USUARIO ' . $this->session->activo->nombre . ' ' . $this->session->activo->ap_paterno . ' ' . $this->session->activo->ap_materno . ' CERRÓ SESIÓN');

	        $this->loginlibrary->salir($dataBitacora);
	    }
        //Redirecciona a inicio de sesión
        redirect('');
    }

}

/* End of file inicio.php */
/* Location: ./application/controllers/inicio.php */ ?>
