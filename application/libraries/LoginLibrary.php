<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class LoginLibrary
{
	private $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
	}
	
	function comprobar()
	{
		return $this->ci->session->userdata('activo');
	}

    function pruebacorreo($username)
    {
        $res = $this->ci->db->query("SELECT username FROM usuario WHERE username = '$username'");
		return $res->num_rows() > 0;
	}


	function insertarBitacora($dataBitacora)
	{
        $this->ci->db->insert('bitacora', $dataBitacora);
	}

	function logout($dataBitacora)
	{
		$this->ci->session->unset_userdata(array('username'));
		$this->ci->db->insert('bitacora', $dataBitacora);		
		$this->ci->session->sess_destroy();	
	}

	function login($username, $password)
	{
		$this->ci->db->select('password');
		$query = $this->ci->db->get_where('usuario', array('username' => $username, 'activo' => 1));
		$row = $query->row();

		if ($query->num_rows() == 1)
		{
			$pass = password_verify($password, $row->password);

			if($pass)
			{
				$this->ci->db->select('u.id_usuario, p.nombre, p.ap_paterno, p.ap_materno, u.rol, u.activo, u.username, p.id_persona');
				$this->ci->db->from('persona p');
				$this->ci->db->join('usuario u', 'p.id_persona = u.id_persona');
				$this->ci->db->where(array('username' => $username));
				$query = $this->ci->db->get();

				// Se crea la sesión
				$this->ci->session->set_userdata('activo', $query->row());
				
				// Regresa que se creo la sesión
				return array(
					'estado' => TRUE,
					'mensaje' => 'Se inició sesión',
					'datos' => $query->row()
				);
			}
			
			return array(
				'estado' => FALSE,
				'mensaje' => 'La contraseña es incorrecta'
			);
		}
		
		return array(
			'estado' => FALSE,
			'mensaje' => 'Usuario no fue encontrado o está inactivo'
		);
	}


	function salir($dataBitacora)
	{
		$this->ci->session->unset_userdata(array('username'));
		$this->ci->db->insert('bitacora', $dataBitacora);		
		$this->ci->session->sess_destroy();	
	}
}