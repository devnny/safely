<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends CI_Model {

	public function getPersona($id_persona)
	{
		$query = $this->db->get_where('persona', array('id_persona' => $id_persona));

		if($query -> num_rows() == 1)
		{
			$row = $query->row();
			return $row;
		}
		return FALSE;
	}

	public function get($cosa)
	{
		$query = $this->db->get($cosa);

		if($query->num_rows() > 0)
	    {
	    	return $query->result_array();
	    }

	    return array(
			'resultado' => FALSE,
			'mensaje' => 'No hay datos'
		);	    
	}

	public function getUno($uno, $id_uno, $tabla)
	{
		$query = $this->db->get_where($tabla, array($uno => $id_uno));

		if($query -> num_rows() == 1)
		{
			$row = $query->row();
			return $row;
		}
		return FALSE;
	}

	public function editTablaUno($uno, $id_uno, $tablaUno, $dataUno, $dataBitacora)
	{
		$this->db->trans_begin();
        $this->db->where($uno, $id_uno);
        $this->db->update($tablaUno, $dataUno);
        $this->db->insert('bitacora', $dataBitacora);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();

            return array(
                'resultado' => FALSE,
                'mensaje' => 'No se pudieron actualizar los datos'
            );
        }

        $this->db->trans_commit();

        return array(
            'resultado' => TRUE, 
            'mensaje' => 'Datos Actualizados'
        );
	}

	function ultimo_id($tabla)
	{
		$this -> db -> select_max('id_'.$tabla);
		$query = $this -> db -> get($tabla);

		if($query -> num_rows() == 1)
		{
			$row = $query->row();
			return $row;
		}
		else FALSE;		
	}

	public function ingresarTabla($dataBitacora, $dataTabla, $tabla)
	{

		$this->db->trans_begin();
		$this->db->insert($tabla, $dataTabla);
		$this->db->insert('bitacora', $dataBitacora);
         
      	if ($this->db->trans_status() === FALSE)
      	{ 
         	$this->db->trans_rollback(); 

		    return array(
				'resultado' => FALSE,
				'mensaje' => 'No se pudieron insertar los datos'
			);
      	}
      	else
      	{
         	$this->db->trans_commit();
         	
         	return array(
				'resultado' => TRUE, 
				'mensaje' => 'Datos creados correctamente'
			);
      	}
    }

    public function getUsuarios()
	{
		$this->db->select('u.id_usuario, p.id_persona, p.telefono,  p.correo, u.rol, p.nombre, p.ap_paterno, p.ap_materno, u.activo' );
	    $this->db->from('persona p');
	    $this->db->join('usuario u', 'p.id_persona = u.id_persona');
	    // $this->db->where('u.activo',$parametro);
	    $query = $this->db->get();

	    if($query->num_rows() > 0)
	    {
	    	return $query->result_array();
	    }

	    return array(
			'resultado' => FALSE,
			'mensaje' => 'No hay usuarios'
		);
	}

	public function getPersonalUno($id_usuario)
	{
		$this->db->select('u.id_usuario, p.*');
	    $this->db->from('persona p');
	    $this->db->join('usuario u', 'p.id_persona = u.id_persona');
		$this->db->where('u.id_usuario', $id_usuario);
        $query = $this->db->get();

        if($query -> num_rows() == 1)
		{
			$row = $query->row();
			return $row;
		}
		return FALSE;
	}

	// Función que verifica si el correo del usuario ya existe
	public function existeUsuario($username)
	{
		// $this->db->select('id_usuario');
		$this->db->where('username', $username);
		$query = $this->db->get('usuario');

		if($query->num_rows() == 0)
		{
			return $resultado = array(
				'resultado' => TRUE,
				'mensaje' => 'EXITO'
			);
		}

		return array(
			'resultado' => FALSE,
			'mensaje' => 'El nombre de usuario ya está registrado, intente con otro'
		);
	}

	// Funcion que inserta nuevo usuario
	public function ingresarUsuario($dataPersona, $dataUsuario, $dataBitacora)
   	{
		//Inicia la transacción
		$this->db->trans_begin();
		//Se inserta en la tabla persona, los datos correspondientes
		$this->db->insert('persona', $dataPersona);
		//Intenta insertar el usuario.
		$this->db->insert('usuario', $dataUsuario);
		$this->db->insert('bitacora', $dataBitacora);
         
      	if ($this->db->trans_status() === FALSE)
      	{
         	//Hubo errores en la consulta, entonces se cancela la transacción.   
         	$this->db->trans_rollback(); 

		    return array(
				'resultado' => FALSE,
				'mensaje' => 'No se pudo insertar el usuario'
			);
      	}
      	else
      	{
     		//Todas las consultas se hicieron correctamente.
         	$this->db->trans_commit();
         	
         	return array(
				'resultado' => TRUE, 
				'mensaje' => 'Usuario creado correctamente'
			);
      	}
   	}

   	public function ingresarUsuarioWeb($dataPersona, $dataUsuario, $dataBitacora, $dataClave)
   	{

		$this->db->trans_begin();
		$this->db->insert('persona', $dataPersona);
		$this->db->insert('usuario', $dataUsuario);
		$this->db->insert('bitacora', $dataBitacora);
		$this->db->insert('clave', $dataClave);
         
      	if ($this->db->trans_status() === FALSE)
      	{
         	$this->db->trans_rollback(); 

		    return array(
				'resultado' => FALSE,
				'mensaje' => 'No se pudo hacer el registro'
			);
      	}
      	else
      	{
         	$this->db->trans_commit();
         	
         	return array(
				'resultado' => TRUE, 
				'mensaje' => 'Registro exitoso, se te enviara un correo para confirmar.'
			);
      	}
   	}

   	public function updateUserDos($id_usuario, $id_persona, $dataUsuario, $dataPersona, $dataBitacora)
	{
		$this->db->trans_begin();
        $this->db->where('id_usuario', $id_usuario);
        $this->db->update('usuario', $dataUsuario);
        $this->db->where('id_persona', $id_persona);
        $this->db->update('persona', $dataPersona);
        $this->db->insert('bitacora', $dataBitacora);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();

            return array(
                'resultado' => FALSE,
                'mensaje' => 'No se pudo actualizar el usuario'
            );
        }

        $this->db->trans_commit();

        return array(
            'resultado' => TRUE, 
            'mensaje' => 'Usuario Actualizado'
        );
	}

	public function updateUser($id_usuario, $dataUsuario, $dataBitacora)
	{
		$this->db->trans_begin();
        $this->db->where('id_usuario', $id_usuario);
        $this->db->update('usuario', $dataUsuario);
        $this->db->insert('bitacora', $dataBitacora);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();

            return array(
                'resultado' => FALSE,
                'mensaje' => 'No se pudo actualizar el usuario'
            );
        }

        $this->db->trans_commit();

        return array(
            'resultado' => TRUE, 
            'mensaje' => 'Usuario Actualizado'
        );
	}

	public function getFacturas()
	{
		$this->db->select('*');
	    $this->db->from('factura f');
	    $this->db->join('proveedor p', 'p.id_proveedor = f.fk_proveedor');
	    $this->db->join('sucursal s', 's.id_sucursal = f.fk_sucursal');

	    $query = $this->db->get();

	    if($query->num_rows() > 0)
	    {
	    	return $query->result_array();
	    }

	    return array(
			'resultado' => FALSE,
			'mensaje' => 'No hay facturas'
		);
	}

	public function deleteUno($uno, $id_uno, $tablaUno, $dataBitacora)
	{
		$this->db->trans_begin();
        $this->db->where($uno, $id_uno);
        $this->db->delete($tablaUno);
        $this->db->insert('bitacora', $dataBitacora);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();

            return array(
                'resultado' => FALSE,
                'mensaje' => 'No se pudieron eliminar los datos'
            );
        }

        $this->db->trans_commit();

        return array(
            'resultado' => TRUE, 
            'mensaje' => 'Datos Eliminados'
        );
	}

	public function buscarUno($uno, $id_uno, $tabla)
	{
		$query = $this->db->get_where($tabla, array($uno => $id_uno));

		if($query -> num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}

}

/* End of file categorias.php */
/* Location: ./application/models/categorias.php */