<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no", shrink-to-fit=no">

    <title>SAFELY</title>
    <?php echo link_tag('assets/img/safely.png', 'shortcut icon', 'image/x-ico') ?>

    <?php echo link_tag('assets/css/0.7.2_css_bulma.min.css') ?>
    <?php echo link_tag('assets/css/mycss.css') ?>

    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/fontawesome.com_v5.3.1.js'></script>
  </head>
  
  <body>
    <section class="hero is-danger is-fullheight">
      <!-- Hero head: will stick at the top -->
      <div class="hero-head">
        <header class="navbar">
          <div class="container">
            <div class="navbar-brand">
              <a class="navbar-item">
                <img src="<?php echo base_url() ?>/assets/img/safely_texto_claro.png" alt="Logo" >
              </a>
              <span class="navbar-burger burger" data-target="navbarMenuHeroC">
                <span></span>
                <span></span>
                <span></span>
              </span>
            </div>
            <div id="navbarMenuHeroC" class="navbar-menu">
              <div class="navbar-end">
                <a class="navbar-item">
                  Cerca de tí
                </a>
                <a class="navbar-item" href="<?php echo base_url() ?>registrar">
                  Registra un Lugar
                </a>
                <a class="navbar-item">
                  Buscar
                </a>
                <span class="navbar-item">
                  <a class="button is-danger is-inverted modal-button" data-target="modal-acerca" aria-haspopup="true">
                    <span class="icon">
                      <i class="fab fa-info-circle"></i>
                    </span>
                    <span>Acerca de SAFELY</span>
                  </a>
                </span>
              </div>
            </div>
          </div>
        </header>
      </div>

      <!-- Hero content: will be in the middle -->
      <div class="hero-body">
        <div class="container has-text-centered">
          <img src="<?php echo base_url(); ?>/assets/img/safely_texto_claro.png" alt="">
          <h2 class="subtitle">
            Safely es una plataforma donde podrás encontrar lugares seguros (negocios, locales) y publicos donde resguardarte si te sientes en una situación de peligro.
          </h2>
          <div class="buttons has-addons is-centered">
            <a class="button is-medium is-danger is-inverted is-outlined">
              <span class="icon">
                <i class="fas fa-map-marked-alt"></i>
              </span>
              <span>Cerca de tí</span>
            </a>

            <a class="button is-medium is-danger is-inverted">
              <span class="icon">
                <i class="fas fa-map-pin"></i>
              </span>
              <span>Registra un lugar</span>
            </a>
          </div>
        </div>
      </div>

      <!-- Hero footer: will stick at the bottom -->
      <div class="hero-foot">
        <div class="content has-text-centered">
          <p>
            <strong>SAFELY</strong> by <a href="#">Murdok</a>.
          </p>
        </div>
      </div>
    </section>

    <div class="modal" id="modal-acerca">
      <div class="modal-background"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">Modal title</p>
          <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
          <!-- Content ... -->
        </section>
        <footer class="modal-card-foot">
          <button class="button is-success">Save changes</button>
          <button class="button">Cancel</button>
        </footer>
      </div>
    </div>

    <script src="https://bulma.io/vendor/clipboard-1.7.1.min.js"></script>
    <script src="https://bulma.io/vendor/js.cookie-2.1.4.min.js"></script>
    <script src="https://bulma.io/lib/main.js?v=201901250817"></script>
  </body>
</html>