<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no", shrink-to-fit=no">

		<title>SAFELY - Login</title>
		<?php echo link_tag('assets/img/safely.png', 'shortcut icon', 'image/x-ico') ?>

   		<?php echo link_tag('assets/css/0.7.2_css_bulma.min.css') ?>
    	<?php echo link_tag('assets/css/mycss.css') ?>

    	<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/fontawesome.com_v5.3.1.js'></script>
	</head>
	
	<body id="base_url" value="<?php echo base_url(); ?>">
		
		<section class="hero is-danger is-fullheight">
	      <!-- Hero head: will stick at the top -->
	      <div class="hero-head">
	        <header class="navbar">
	          <div class="container">
	            <div class="navbar-brand">
	              <a class="navbar-item">
	                <img src="<?php echo base_url() ?>/assets/img/safely_texto_claro.png" alt="Logo" >
	              </a>
	              <span class="navbar-burger burger" data-target="navbarMenuHeroC">
	                <span></span>
	                <span></span>
	                <span></span>
	              </span>
	            </div>
	            <div id="navbarMenuHeroC" class="navbar-menu">
	              <div class="navbar-end">
	                <a class="navbar-item">
	                  Cerca de tí
	                </a>
	                <a class="navbar-item" href="<?php echo base_url() ?>registrar">
	                  Registra un Lugar
	                </a>
	                <a class="navbar-item">
	                  Buscar
	                </a>
	                <span class="navbar-item">
	                  <a class="button is-danger is-inverted modal-button" data-target="modal-acerca" aria-haspopup="true">
	                    <span class="icon">
	                      <i class="fab fa-info-circle"></i>
	                    </span>
	                    <span>Acerca de SAFELY</span>
	                  </a>
	                </span>
	              </div>
	            </div>
	          </div>
	        </header>
	      </div>

	      <!-- Hero content: will be in the middle -->
	      <div class="hero-body">
	        <div class="container has-text-centered">

	        	<?php echo validation_errors('<p class="text-danger">', '</p>'); ?>
		        <?php
		        	$error = $this->session->flashdata('error');
		            if ($error) { ?>
		            <div class="bg-danger text-white" align="center">
		                <?php echo $error ?>
		            </div><br/>
		        <?php } ?>

	        	<div class="tabs is-centered is-toggle">
				  <ul>
				    <li id="btnIngresar" class="is-active">
				      <a>
				        <span class="icon is-small"><i class="fas fa-sign-in-alt" aria-hidden="true"></i></span>
				        <span>Ingresar</span>
				      </a>
				    </li>
				    <li id="btnRegistrar">
				      <a>
				        <span class="icon is-small"><i class="fas fa-keyboard" aria-hidden="true"></i></span>
				        <span>Registrarse</span>
				      </a>
				    </li>
				  </ul>
				</div>

				<!--  -->
				<div class="column is-one-third is-offset-one-third has-background-transparent" id="divIngresar">
					<form class="form-signin" autocomplete="off" action="<?php echo base_url(); ?>entrar" method="post">
						<!--<p class="has-text-centered is-size-6">Ingresa tus datos</p>-->
					    <div class="field">
					    	<label class="label has-text-white has-text-left">Email:</label>
							<p class="control has-icons-left">
							    <input class="input has-text-danger" type="email" placeholder="Email" required name="usuario">
							    <span class="icon is-small is-left">
							      <i class="fas fa-envelope"></i>
							    </span>
						 	</p>
						</div>
						<div class="field">
							<label class="label has-text-white has-text-left">Contraseña:</label>
						  	<p class="control has-icons-left">
							    <input class="input has-text-danger" type="password" placeholder="Password" required name="contrasenia">
							    <span class="icon is-small is-left">
							      <i class="fas fa-lock"></i>
							    </span>
						  	</p>
						</div>

						<div class="field">
							<div class="buttons has-addons is-centered">
								<button class="button is-danger is-inverted" type="submit">
						    		<span>Ingresar</span><span class="icon is-small"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i></span>
						    	</button>
							</div>
						</div>
					</form>
				</div>

				<div class="column is-one-third is-offset-one-third has-background-transparent" id="divRegistrar">
					<form class="form-signin" id="formRegistrar">
					    <div class="field">
					    	<label class="label has-text-white has-text-left">Email</label>
							<p class="control has-icons-left">
							    <input class="input has-text-danger" type="email" placeholder="Email" required name="mail">
							    <span class="icon is-small is-left">
							      <i class="fas fa-envelope"></i>
							    </span>
						 	</p>
						</div>
						<div class="field">
							<label class="label has-text-white has-text-left">Contraseña</label>
						  	<p class="control has-icons-left">
							    <input class="input has-text-danger" type="password" placeholder="Password" required name="pass">
							    <span class="icon is-small is-left">
							      <i class="fas fa-lock"></i>
							    </span>
								<p class="help">Minimo 6 caracteres</p>
						  	</p>
						</div>
						<div class="field">
							<label class="label has-text-white has-text-left">Confirmar Contraseña</label>
						  	<p class="control has-icons-left">
							    <input class="input has-text-danger" type="password" placeholder="Password" required name="pass2">
							    <span class="icon is-small is-left">
							      <i class="fas fa-lock"></i>
							    </span>
						  	</p>
						</div>

						<div class="field">
							<div class="buttons has-addons is-centered">
								<button class="button is-danger is-inverted" type="submit">
						    		<span>Registrarse</span><span class="icon is-small"><i class="fas fa-keyboard" aria-hidden="true"></i></span>
						    	</button>
							</div>
						</div>
					</form>
				</div>

	        </div>
	      </div>

	      <!-- Hero footer: will stick at the bottom -->
	      <div class="hero-foot">
	        <div class="content has-text-centered">
	          <p>
	            <strong>SAFELY</strong> by <a href="#">Murdok</a>.
	          </p>
	        </div>
	      </div>
	    </section>

	    <div class="modal" id="modal-acerca">
	      <div class="modal-background"></div>
	      <div class="modal-card">
	        <header class="modal-card-head">
	          <p class="modal-card-title">Modal title</p>
	          <button class="delete" aria-label="close"></button>
	        </header>
	        <section class="modal-card-body">
	          <!-- Content ... -->
	        </section>
	        <footer class="modal-card-foot">
	          <button class="button is-success">Save changes</button>
	          <button class="button">Cancel</button>
	        </footer>
	      </div>
	    </div>
		

		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="https://bulma.io/vendor/clipboard-1.7.1.min.js"></script>
   	 	<script src="https://bulma.io/vendor/js.cookie-2.1.4.min.js"></script>
    	<script src="https://bulma.io/lib/main.js?v=201901250817"></script>
    	<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/sweetalert.min.js'></script>
    	<script type="text/javascript">
			$(document).ready(function()
			{
				var baseurl = $('#base_url').attr('value');

				$('#btnRegistrar').click(function() {
					$('#divRegistrar').show();
					$('#btnRegistrar').addClass("is-active");

					$('#divIngresar').hide();
					$('#btnIngresar').removeClass("is-active");
				});

				$('#btnIngresar').click(function() {
					$('#divRegistrar').hide();
					$('#btnRegistrar').removeClass("is-active");
					
					$('#divIngresar').show();
					$('#btnIngresar').addClass("is-active");
				});

				$('#formRegistrar').on('submit', (function(e)
				{
					e.preventDefault();
					var cat = $(this).attr('value');
					//Ajax para registro

					$.ajax({
						url: baseurl+'insertNuevo/',
						type:'POST',
						data: $(this).serialize(),
						dataType: 'json',
						success: function(respuesta)
						{
							if (respuesta.resultado == true)
				           {
				                swal({
				                    title: "Registro Exitoso",
			                   		icon: "success",		
				                }).then(function(){
				                   	location.reload();
				                });
				           }else 
				           {
				               swal('Error', respuesta.datos, "error");
				           }
						}
					})
			       	//Fin ajax
				}));
				//Fin script registrar

			});
		</script>
	</body>
</html>